const fs = require("fs"),
    _ = require("lodash"),
    util = require("util");

class Settings {
    constructor(path, options) {
        this.path = path;
        options = _.assign({}, {pretty_print_file: false, pretty_print_spacing: 4}, options);
        this.pretty_print_file = options.pretty_print_file === true;
        this.pretty_print_spacing = util.isNumber(options.pretty_print_spacing) ? options.pretty_print_spacing : 4;
        this.raw_data = {};
        this._proxy = null;

        require("mkdirp")(require("path").dirname(path), err => {
            if (!err) {
                require("touch")(path, err => {
                    this.reload();
                });
            } else {
                throw err;
            }
        });
    }

    save() {
        fs.writeFileSync(this.path, JSON.stringify(this.raw_data, null, (this.pretty_print_file ? this.pretty_print_spacing : 0)));
        return this;
    }

    reload() {
        try {
            this.raw_data = JSON.parse(fs.readFileSync(this.path));
        } catch (e) {
            this.raw_data = {};
            this.save();
        }
        return this;
    }

    get(name, defaultValue) {
        this.reload();
        let ret = this.raw_data[name];
        if (util.isNullOrUndefined(ret)) {
            this.set(name, defaultValue);
            return defaultValue;
        } else {
            return ret;
        }
    }

    set(name, value) {
        this.raw_data[name] = value;
        this.save();
        return this;
    }

    delete(name) {
        delete this.raw_data[name];
        this.save();
        return this;
    }

    setAll(toSet) {
        if (!util.isObject(toSet)) throw new Error("Expecting an object of keys/values to set ({option: \"value\"})");

        _.assign(this.raw_data, toSet);
        this.save();
    }

    getAll() {
        let names = _.compact(arguments);
        if (util.isArray(names[0])) names = names[0];

        this.reload();

        let toReturn = {};
        for (let i = 0; i < names.length; i++) {
            if (util.isString(names[i])) {
                toReturn[names[i]] = this.raw_data[names[i]];
            } else if (util.isObject(names[i]) && util.isString(names[i].key)) {
                toReturn[names[i].key] = this.get(names[i].key, names[i].defaultValue);
            }
        }

        return toReturn;
    }

    get proxy() {
        if (typeof Proxy !== 'undefined') {
            let self = this;
            if (util.isNullOrUndefined(this._proxy)) {
                this._proxy = new Proxy(self, {
                    get: (target, key) => {
                        return target.get(key);
                    },
                    set: (target, property, value, receiver) => {
                        return target.set(property, value);
                    }
                });
            }
            return this._proxy;
        } else {
            throw new Error("Proxy is not supported by this environment");
        }
    }
}

module.exports = Settings;